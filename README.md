# Microsites Sdk

Software development kit to connect with the Microsites's API

## Installation
You should add PlacetoPay repository:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://dev.placetopay.com/repository"
        }
    ]
}
```

Then, you can install the package via composer:
```
composer require placetopay/microsites-sdk
```

## Usage
The first thing to do is create an instance of the Gateway class and pass the credentials (login, apiKey, url)
in the parameter as an array. Example:
```
return new Gateway([
    'login' => 'user_placetopay',
    'apiKey' => 'P2P123#',
    'url' => 'https://dev.placetopay.com',
]);
```

## Available methods

### createMicrosite():
To create a microsite, you must call the createMicrosite() method from the Gateway class instance and pass as a parameter an
instance of the MicrositeTransaction class that receives an array with the request fields. Example:

```
$data = new MicrositeTransaction([
    'name' => 'exampleName',
    'alias' => 'exampleAlias,
    'type' => OptionsFields::OPEN_TYPE,
    'sites' => [1, 2, 3, 4, 10],
    'allowPartial' => true,
    'categories' => ['test_tecnologia'],
    'loginFields' => 1,
    'version' => OptionsFields::VERSION_2,
    'paymentExpiration' => 10,
]);

$response = $gateway->createMicrosite($data);
```

## Responses
The response obtained in each of the available methods always belongs to a 'Status' type entity that is understood by
the projects that incorporate the Placetopay 'Base' package. Which consists of: status, reason, message and date.
```
{
  +status: "OK"
  +reason: "00"
  +message: "{"status":{"status":"OK","reason":201,"message":"Microsite created successfully",
            "date":"2022-11-24T17:03:15-05:00"},
            "data":{"id":706,"url":"https:\/\/dev.placetopay.com\/microsites\/c120964463a1efc84e8c"}}"
  +date: "2022-11-24T16:25:39+00:00"
}
```

To get only the status
```
$response->status
```

To get only the reason
```
$response->reason
```

To get only the message
```
$response->message
```

To get only the date
```
$response->date
```

#### To obtain some specific data from the message, you must decode the JSON of the message property and access the property of the data that is required.
Example
```
json_decode($response->message)->data->url   //To get the url of a created microsite
```

## Constants

The optionsFile class contains the following constants:
```
public const OPEN_TYPE = 'OPEN';
public const CLOSED_TYPE = 'CLOSED';

public const VERSION_1 = 'v1';
public const VERSION_2 = 'v2';
```
It is recommended to use them when giving a value to the "type" and "version" fields.

## Important

* The biggest difference between creating an open microsite and a closed one is that a payment order reference is 
required for a closed microsite, while an open microsite does not. It is important to highlight that you must indicate 
what type of microsite you want to create in the "type" field of the Request.

* Remember that available methods must be passed an instance of MicrositeTransaction as a parameter to avoid an exception or error.

* Note that the "name" and "alias" fields are unique to a microsite associated with a given site and creating microsites
with the same name or alias will return a failed status.


#### For more information about the microsites API see the documentation [Here](https://ptp-microsites.stoplight.io/docs/microsites-api-docs/YXBpOjg3Mzg4MjE-microsites)