<?php

namespace Pabon\MicrositesSdk\Constants;

class Endpoints
{
    public const BASE = '/microsites';
    public const CREATE_MICROSITE = '/api/microsites';
    public const AUTHENTICATION = '/auth';
}
