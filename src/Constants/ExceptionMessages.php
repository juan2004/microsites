<?php

namespace Pabon\MicrositesSdk\Constants;

class ExceptionMessages
{
    public const NAME_FIELD = 'The field name is mandatory and must be of type string greater than or equal to one character';
    public const ALIAS_FIELD = 'The field alias is mandatory and must be of type string greater than or equal to one character';
    public const TYPE_FIELD = 'The field type is mandatory and must be of type OPEN or CLOSED';
    public const CATEGORIES_FIELD = 'The field categories is mandatory and must be of type array string';
    public const SITES_FIELD = 'The field sites is mandatory and must be of type array integer';
    public const LOGIN_FIELDS_FIELD = 'The field loginFields is mandatory and must be of type integer';
    public const ALLOW_PARTIAL_FIELD = 'The field allowPartial is mandatory and must be of type boolean';
}
