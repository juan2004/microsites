<?php

namespace Pabon\MicrositesSdk\Constants;

class Fields
{
    public const AUTH = 'auth';
    public const NAME = 'name';
    public const ALIAS = 'alias';
    public const TYPE = 'type';
    public const CATEGORIES = 'categories';
    public const SITES = 'sites';
    public const LOGIN_FIELDS = 'loginFields';
    public const ALLOW_PARTIAL = 'allowPartial';
    public const VERSION = 'version';
    public const PAYMENT_EXPIRATION = 'paymentExpiration';
}
