<?php

namespace Pabon\MicrositesSdk\Constants;

class OptionsFields
{
    public const OPEN_TYPE = 'OPEN';
    public const CLOSED_TYPE = 'CLOSED';

    public const VERSION_1 = 'v1';
    public const VERSION_2 = 'v2';

    public const PAYMENT_EXPIRATION_DEFAULT = 10;
}
