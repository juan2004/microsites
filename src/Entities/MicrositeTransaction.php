<?php

namespace Pabon\MicrositesSdk\Entities;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use PlacetoPay\Base\Traits\LoaderTrait;
use Pabon\MicrositesSdk\Constants\ExceptionMessages;
use Pabon\MicrositesSdk\Constants\OptionsFields;
use Pabon\MicrositesSdk\Exceptions\MicrositesSdkException;

class MicrositeTransaction extends Transaction
{
    use LoaderTrait;

    protected ?array $additional = [];

    protected string $name;
    protected string $alias;
    protected string $type;
    protected array $categories;
    protected array $sites;
    protected int $loginFields;
    protected bool $allowPartial;
    protected string $version = OptionsFields::VERSION_2;
    protected int $paymentExpiration = OptionsFields::PAYMENT_EXPIRATION_DEFAULT;

    protected array $auth;

    protected array $toArrayProperties = [
        'name',
        'alias',
        'type',
        'categories',
        'sites',
        'loginFields',
        'allowPartial',
        'version',
        'paymentExpiration',
        'additional',
    ];

    public function __construct(array $data)
    {
        $this->status = Status::quick(Status::ST_PENDING, ReasonCodes::PENDING_TRANSACTION);
        $this->id = 'system-'.uniqid();

        $this->load($data, ['name', 'alias', 'type', 'categories', 'sites', 'loginFields', 'allowPartial',
            'version', 'paymentExpiration', 'additional', 'auth', ]);
    }

    public function additional(?string $key = null, $default = null)
    {
        if ($key) {
            return $this->additional[$key] ?? $default;
        }

        return $this->additional;
    }

    public function setAdditional(?array $additional): self
    {
        $this->additional = $additional;

        return $this;
    }

    public function mergeAdditional(array $additionalExtra): self
    {
        $this->additional = array_replace($this->additional, $additionalExtra);

        return $this;
    }

    public function getName(): string
    {
        if (isset($this->name) && strlen($this->name) >= 1) {
            return $this->name;
        } else {
            throw MicrositesSdkException::forDataNotProvided(ExceptionMessages::NAME_FIELD);
        }
    }

    public function getAlias(): string
    {
        if (isset($this->alias) && strlen($this->alias) >= 1) {
            return $this->alias;
        } else {
            throw MicrositesSdkException::forDataNotProvided(ExceptionMessages::ALIAS_FIELD);
        }
    }

    public function getType(): string
    {
        if (isset($this->type)) {
            return $this->type;
        } else {
            throw MicrositesSdkException::forDataNotProvided(ExceptionMessages::TYPE_FIELD);
        }
    }

    public function getCategories(): array
    {
        if (isset($this->categories)) {
            return $this->categories;
        } else {
            throw MicrositesSdkException::forDataNotProvided(ExceptionMessages::CATEGORIES_FIELD);
        }
    }

    public function getSites(): array
    {
        if (isset($this->sites)) {
            return $this->sites;
        } else {
            throw MicrositesSdkException::forDataNotProvided(ExceptionMessages::SITES_FIELD);
        }
    }

    public function getLoginFields(): int
    {
        if (isset($this->loginFields)) {
            return $this->loginFields;
        } else {
            throw MicrositesSdkException::forDataNotProvided(ExceptionMessages::LOGIN_FIELDS_FIELD);
        }
    }

    public function isAllowPartial(): bool
    {
        if (isset($this->allowPartial)) {
            return $this->allowPartial;
        } else {
            throw MicrositesSdkException::forDataNotProvided(ExceptionMessages::ALLOW_PARTIAL_FIELD);
        }
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getPaymentExpiration(): int
    {
        return $this->paymentExpiration;
    }

    public function getAuth(): array
    {
        return $this->auth;
    }

    public function setAuth(array $dataAuth): void
    {
        $this->auth = $dataAuth;
    }
}
