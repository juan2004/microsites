<?php

namespace Pabon\MicrositesSdk\Entities;

use GuzzleHttp\ClientInterface;
use PlacetoPay\Tangram\Entities\BaseSettings;
use Psr\SimpleCache\CacheInterface;

class Settings extends BaseSettings
{
    private const ALGORITHM = 'sha256';

    private string $nonce;
    private string $seed;

    public function login(): string
    {
        return $this->get('login');
    }

    public function apiKey(): string
    {
        return $this->get('apiKey');
    }

    public function providerName(): string
    {
        return $this->get('providerName');
    }

    public function url(): string
    {
        return $this->get('url');
    }

    public function client(): ClientInterface
    {
        return $this->get('client');
    }

    public function cache(): CacheInterface
    {
        return $this->get('cache');
    }

    public function simulatorMode(): bool
    {
        return $this->get('simulatorMode');
    }

    public function loggerSettings(): ?array
    {
        return $this->get('logger');
    }

    public function nonce()
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $this->nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } elseif (function_exists('random_bytes')) {
            $this->nonce = bin2hex(random_bytes(16));
        } else {
            $this->nonce = mt_rand();
        }

        return $this->nonce;
    }

    public function seed(): string
    {
        $this->seed = date('c');

        return $this->seed;
    }

    public function tranKey(): string
    {
        return base64_encode(
            hash(self::ALGORITHM, $this->nonce().$this->seed().$this->apiKey(), true)
        );
    }

    public function auth(): array
    {
        return [
            'login' => $this->login(),
            'tranKey' => $this->trankey(),
            'nonce' => base64_encode($this->nonce),
            'seed' => $this->seed(),
        ];
    }
}
