<?php

namespace Pabon\MicrositesSdk\Exceptions;

class ParserException extends MicrositesSdkException
{
    public static function invalidOperation(string $operation): self
    {
        return new self(sprintf('Operation %s is not supported', $operation));
    }
}
