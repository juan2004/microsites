<?php

namespace Pabon\MicrositesSdk;

use PlacetoPay\Base\Entities\Status;
use Pabon\MicrositesSdk\Constants\Operations;
use Pabon\MicrositesSdk\Entities\MicrositeTransaction;
use Pabon\MicrositesSdk\Exceptions\ParserException;
use Pabon\MicrositesSdk\Support\ParserManager;
use Pabon\MicrositesSdk\Entities\Settings;
use Pabon\MicrositesSdk\Support\SettingsResolver;
use PlacetoPay\Tangram\Carriers\RestCarrier;
use PlacetoPay\Tangram\Events\Dispatcher;
use PlacetoPay\Tangram\Exceptions\InvalidSettingException;
use PlacetoPay\Tangram\Listeners\HttpLoggerListener;
use PlacetoPay\Tangram\Services\BaseGateway;

class Gateway extends BaseGateway
{
    protected Settings $settings;
    protected RestCarrier $carrier;
    protected ParserManager $parserManager;

    /**
     * @throws InvalidSettingException
     */
    public function __construct(array $settings)
    {
        $this->settings = new Settings($settings, SettingsResolver::create($settings));
        $this->carrier = new RestCarrier($this->settings->client());
        $this->parserManager = new ParserManager($this->settings);

        $this->addEventDispatcher();
    }

    public function createMicrosite(MicrositeTransaction $transaction): Status
    {
        $transaction->setAuth($this->settings->auth());

        return $this->process(Operations::CREATE_MICROSITE, $transaction)->status();
    }

    private function process(string $operation, MicrositeTransaction $transaction): MicrositeTransaction
    {
        try {
            $parser = $this->parserManager->getParser($operation);
            $carrierDataObject = $this->parseRequest($operation, $transaction, $parser);
            $this->carrier->request($carrierDataObject);
            $this->parseResponse($carrierDataObject, $parser);

            return $transaction;
        } catch (ParserException $e) {
            throw ParserException::invalidOperation($operation);
        }
    }

    public function settings(): Settings
    {
        return $this->settings;
    }

    /**
     * @throws InvalidSettingException
     */
    protected function addEventDispatcher(): void
    {
        // Add Events Required
        if ($loggerSettings = $this->settings->loggerSettings()) {
            $this->setLoggerContext($loggerSettings);

            $listener = new HttpLoggerListener(
                $loggerSettings,
                $this->settings->providerName(),
                $this->settings->simulatorMode()
            );

            $this->carrier->setDispatcher(Dispatcher::create($listener->getEventsMethodsToDispatcher()));
        }
    }

    protected function setLoggerContext(&$loggerSettings): void
    {
        // Add masking rules for logs
        $loggerSettings['context'] = [
            'request' => [],
        ];
    }
}
