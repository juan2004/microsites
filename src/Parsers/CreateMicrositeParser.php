<?php

namespace Pabon\MicrositesSdk\Parsers;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use Pabon\MicrositesSdk\Constants\Endpoints;
use Pabon\MicrositesSdk\Constants\Fields;
use PlacetoPay\Tangram\Contracts\CarrierDataObjectContract;
use PlacetoPay\Tangram\Contracts\ParserHandlerContract;

class CreateMicrositeParser implements ParserHandlerContract
{
    public function parserRequest(CarrierDataObjectContract $carrierDataObject): array
    {
        $carrierDataObject->setOptions(array_merge([
            'method' => 'POST',
            'endpoint' => Endpoints::BASE.Endpoints::CREATE_MICROSITE,
        ]));

        return [
            'json' => [
                Fields::AUTH => $carrierDataObject->transaction()->getAuth(),
                Fields::NAME => $carrierDataObject->transaction()->getName(),
                Fields::ALIAS => $carrierDataObject->transaction()->getAlias(),
                Fields::TYPE => $carrierDataObject->transaction()->getType(),
                Fields::CATEGORIES => $carrierDataObject->transaction()->getCategories(),
                Fields::SITES => $carrierDataObject->transaction()->getSites(),
                Fields::LOGIN_FIELDS => $carrierDataObject->transaction()->getLoginFields(),
                Fields::ALLOW_PARTIAL => $carrierDataObject->transaction()->isAllowPartial(),
                Fields::VERSION => $carrierDataObject->transaction()->getVersion(),
                Fields::PAYMENT_EXPIRATION => $carrierDataObject->transaction()->getPaymentExpiration(),
            ],
        ];
    }

    public function parserResponse(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickOk(
            ReasonCodes::APPROVED_TRANSACTION,
            $carrierDataObject->response()->getBody()->getContents(),
        ));

        return $carrierDataObject->transaction();
    }

    public function errorHandler(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickFailed(
            ReasonCodes::INVALID_RESPONSE,
            $carrierDataObject->error()->getMessage()
        ));

        return $carrierDataObject->transaction();
    }
}
