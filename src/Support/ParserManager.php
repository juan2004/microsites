<?php

namespace Pabon\MicrositesSdk\Support;

use Pabon\MicrositesSdk\Exceptions\ParserException;
use Pabon\MicrositesSdk\Parsers\CreateMicrositeParser;
use PlacetoPay\Tangram\Entities\BaseSettings;

class ParserManager
{
    private const CREATE_MICROSITE = 'createMicrosite';

    protected const OPERATIONS_PARSERS = [
        self::CREATE_MICROSITE => CreateMicrositeParser::class,
    ];

    protected array $parsers = [];
    protected BaseSettings $settings;

    public function __construct(BaseSettings $settings)
    {
        $this->settings = $settings;
    }

    public function getParser(string $operation)
    {
        $this->validateOperation($operation);

        if (! isset($this->parsers[$operation])) {
            $this->createParser($operation);
        }

        return $this->parsers[$operation];
    }

    protected function createParser(string $operation): void
    {
        $parserName = self::OPERATIONS_PARSERS[$operation];
        $this->parsers[$operation] = new $parserName($this->settings);
    }

    /**
     * @throws ParserException
     */
    protected function validateOperation(string $operation): void
    {
        if (! isset(self::OPERATIONS_PARSERS[$operation])) {
            throw ParserException::invalidOperation($operation);
        }
    }
}
