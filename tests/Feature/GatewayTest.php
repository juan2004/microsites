<?php

namespace Tests\Feature;

use PlacetoPay\Tangram\Exceptions\InvalidSettingException;
use Pabon\MicrositesSdk\Gateway;
use Tests\TestCase;

class GatewayTest extends TestCase
{
    public function testTheLoginIsMandatory(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The required option "login" is missing.');

        $resolver = new Gateway([
            'apiKey' => 'P2P123#',
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testItValidatesLoginIsAString(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The option "login" with value array is expected to be of type "string", but is of type "array".');

        $resolver = new Gateway([
            'login' => ['placetopay'],
            'apiKey' => 'P2P123#',
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testTheApiKeyIsMandatory(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The required option "apiKey" is missing.');

        $resolver = new Gateway([
            'login' => 'placetopay',
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testItValidatesApiKeyIsAString(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The option "apiKey" with value array is expected to be of type "string", but is of type "array".');

        $resolver = new Gateway([
            'login' => 'placetopay',
            'apiKey' => ['string'],
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testTheUrlIsMandatory(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The required option "url" is missing.');

        $resolver = new Gateway([
            'login' => 'placetopay',
            'apiKey' => 'P2P123#',
        ]);
    }

    public function testItValidatesUrlIsAString(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The option "url" with value array is expected to be of type "string", but is of type "array".');

        $resolver = new Gateway([
            'login' => 'placetopay',
            'apiKey' => ['string'],
            'url' => ['https://dev.placetopay.com'],
        ]);
    }
}
