<?php

namespace Tests\Feature;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use Pabon\MicrositesSdk\Constants\OptionsFields;
use Pabon\MicrositesSdk\Entities\MicrositeTransaction;
use Pabon\MicrositesSdk\Gateway;
use PlacetoPay\Tangram\Mock\TestLogger;
use Tests\TestCase;

class OperationsTest extends TestCase
{
    private function gateway(): Gateway
    {
        return new Gateway([
            'login' => 'user_placetopay',
            'apiKey' => 'P2P123#',
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    private function dataRequest(string $type): MicrositeTransaction
    {
        return new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => $type,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]);
    }

    public function testItCanCreateAnOpenMicrositeSuccessful(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->createMicrosite($this->dataRequest(OptionsFields::OPEN_TYPE));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertEquals(Status::ST_OK, $response->status);
        $this->assertEquals(ReasonCodes::APPROVED_TRANSACTION, $response->reason);
    }

    public function testItCanCreateAClosedMicrositeSuccessful(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->createMicrosite($this->dataRequest(OptionsFields::CLOSED_TYPE));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertEquals(Status::ST_OK, $response->status);
        $this->assertEquals(ReasonCodes::APPROVED_TRANSACTION, $response->reason);
    }

    public function testItCanNotCreateAMicrositeFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => 'test',
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
        ]));

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => 'test',
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertEquals(Status::ST_FAILED, $response->status);
        $this->assertEquals(ReasonCodes::INVALID_RESPONSE, $response->reason);
    }

    public function testItCanDoLogOfCreationOfAnOpenMicrosite(): void
    {
        $logger = new TestLogger;

        $gateway = new Gateway([
            'login' => 'user_placetopay',
            'apiKey' => 'P2P123#',
            'url' => 'https://dev.placetopay.com',
            'logger' => [
                'via' => $logger,
                'path' => 'fake/path/microsites-sdk_log',
            ],
        ]);

        $response = $gateway->createMicrosite($this->dataRequest(OptionsFields::OPEN_TYPE));

        $this->assertInstanceOf(TestLogger::class, $logger);
    }

    public function testItCanDoLogOfCreationOfAClosedMicrosite(): void
    {
        $logger = new TestLogger;

        $gateway = new Gateway([
            'login' => 'user_placetopay',
            'apiKey' => 'P2P123#',
            'url' => 'https://dev.placetopay.com',
            'logger' => [
                'via' => $logger,
                'path' => 'fake/path/microsites-sdk_log',
            ],
        ]);

        $response = $gateway->createMicrosite($this->dataRequest(OptionsFields::CLOSED_TYPE));

        $this->assertInstanceOf(TestLogger::class, $logger);
    }
}
