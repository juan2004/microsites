<?php

namespace Tests;

use Pabon\MicrositesSdk\Gateway;
use PHPUnit\Framework\TestCase as PHPUnitTestCase;
use PlacetoPay\Tangram\Mock\TestLogger;

class TestCase extends PHPUnitTestCase
{
    protected TestLogger $logger;

    public function createGateway(array $settings = [], bool $mockClient = true): Gateway
    {
        $this->logger = new TestLogger();

        return new Gateway(array_merge([
            'simulatorMode' => $mockClient,
            'logger' => [
                'via' => $this->logger,
                'path' => 'fake/path/microsites-sdk.log',
            ],
        ], $settings));
    }
}
