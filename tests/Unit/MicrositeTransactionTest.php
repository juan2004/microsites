<?php

namespace Tests\Unit;

use Pabon\MicrositesSdk\Constants\ExceptionMessages;
use Pabon\MicrositesSdk\Constants\OptionsFields;
use Pabon\MicrositesSdk\Entities\MicrositeTransaction;
use Pabon\MicrositesSdk\Exceptions\MicrositesSdkException;
use Pabon\MicrositesSdk\Gateway;
use Tests\TestCase;

class MicrositeTransactionTest extends TestCase
{
    private function gateway(): Gateway
    {
        return new Gateway([
            'login' => 'user_placetopay',
            'apiKey' => 'P2P123#',
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    private function dataRequest(string $type): array
    {
        return [
            'name' => 'nameExample',
            'alias' => 'aliasExample',
            'type' => $type,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ];
    }

    public function testItCanCorrectlyAnalyzeTheCreationOfOpenMicrosite(): void
    {
        $request = new MicrositeTransaction($this->dataRequest(OptionsFields::OPEN_TYPE));

        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['name'], $request->getName());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['alias'], $request->getAlias());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['type'], $request->getType());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['sites'], $request->getSites());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['allowPartial'], $request->isAllowPartial());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['categories'], $request->getCategories());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['loginFields'], $request->getLoginFields());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['version'], $request->getVersion());
        $this->assertEquals($this->dataRequest(OptionsFields::OPEN_TYPE)['paymentExpiration'], $request->getPaymentExpiration());
    }

    public function testItCanCorrectlyAnalyzeTheCreationOfClosedMicrosite(): void
    {
        $request = new MicrositeTransaction($this->dataRequest(OptionsFields::CLOSED_TYPE));

        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['name'], $request->getName());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['alias'], $request->getAlias());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['type'], $request->getType());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['sites'], $request->getSites());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['allowPartial'], $request->isAllowPartial());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['categories'], $request->getCategories());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['loginFields'], $request->getLoginFields());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['version'], $request->getVersion());
        $this->assertEquals($this->dataRequest(OptionsFields::CLOSED_TYPE)['paymentExpiration'], $request->getPaymentExpiration());
    }

    public function testTheNameFieldIsMandatory(): void
    {
        $gateway = $this->gateway();

        $this->expectException(MicrositesSdkException::class);

        $this->expectExceptionMessage(ExceptionMessages::NAME_FIELD);

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]));
    }

    public function testTheAliasFieldIsMandatory(): void
    {
        $gateway = $this->gateway();

        $this->expectException(MicrositesSdkException::class);

        $this->expectExceptionMessage(ExceptionMessages::ALIAS_FIELD);

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]));
    }

    public function testTheTypeFieldIsMandatory(): void
    {
        $gateway = $this->gateway();

        $this->expectException(MicrositesSdkException::class);

        $this->expectExceptionMessage(ExceptionMessages::TYPE_FIELD);

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            //'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]));
    }

    public function testTheSitesFieldIsMandatory(): void
    {
        $gateway = $this->gateway();

        $this->expectException(MicrositesSdkException::class);

        $this->expectExceptionMessage(ExceptionMessages::SITES_FIELD);

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]));
    }

    public function testTheAllowPartialFieldIsMandatory(): void
    {
        $gateway = $this->gateway();

        $this->expectException(MicrositesSdkException::class);

        $this->expectExceptionMessage(ExceptionMessages::ALLOW_PARTIAL_FIELD);

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]));
    }

    public function testTheCategoriesFieldIsMandatory(): void
    {
        $gateway = $this->gateway();

        $this->expectException(MicrositesSdkException::class);

        $this->expectExceptionMessage(ExceptionMessages::CATEGORIES_FIELD);

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]));
    }

    public function testTheLoginFieldsFieldIsMandatory(): void
    {
        $gateway = $this->gateway();

        $this->expectException(MicrositesSdkException::class);

        $this->expectExceptionMessage(ExceptionMessages::LOGIN_FIELDS_FIELD);

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'version' => OptionsFields::VERSION_2,
            'paymentExpiration' => 10,
        ]));
    }

    public function testTheVersionFieldIsOptional(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'paymentExpiration' => 10,
        ]));

        $this->assertEquals('OK', $response->status);
    }

    public function testThePaymentExpirationFieldIsOptional(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->createMicrosite(new MicrositeTransaction([
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => OptionsFields::OPEN_TYPE,
            'sites' => [1, 2, 3, 4, 10],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => OptionsFields::VERSION_2,
        ]));

        $this->assertEquals('OK', $response->status);
    }
}
